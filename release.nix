let
  sources    = import ./nix/sources.nix {};
  pkgs       = import sources.nixpkgs {};
  easel      = pkgs.callPackage sources.easel {};
  hmmer      = pkgs.callPackage sources.hmmer { inherit easel; };
  ncbi-blast = pkgs.callPackage sources.ncbi-blast {};
  busco = pkgs.python3Packages.callPackage ./default.nix {
    inherit (pkgs.lib) makeBinPath;
    inherit ncbi-blast hmmer;
  };
in
  busco
