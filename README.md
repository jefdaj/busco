# BUSCO-nix

My fork of [BUSCO](https://gitlab.com/ezlab/busco) to add [Nix](https://nixos.org/) support.
